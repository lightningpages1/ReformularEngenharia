<!-- First Swiper -->
<div class="swiper mySwiper container-fluid" id="esconder-perfil1">
    <div class="swiper-wrapper">
        <div class="swiper-slide position-relative">
            <img src="assets/index/bannersIniciais/2.webp" class="opacity-img-slider"/>
            <div class="position-absolute text-weight-bolder f-size-2 text-color-white text-carousel">NOSSOS SERVICOS</div>
            <button class="position-absolute text-weight-bold btn-swiper">
                <a href="#esconder-perfil2" class="tagA-no-style">
                    Ver Serviços
                </a>
            </button>
        </div>
        <div class="swiper-slide position-relative">
            <img src="assets/index/bannersIniciais/1.webp" class="opacity-img-slider"/>
            <div class="position-absolute text-weight-bolder f-size-2 text-color-white text-carousel">QUEM SOMOS</div>
            <button class="position-absolute text-weight-bold btn-swiper">
                <a href="#esconder-perfil3" class="tagA-no-style">
                    Saber Mais
                </a>
            </button>
        </div>
        <div class="swiper-slide position-relative">
            <img src="assets/index/bannersIniciais/4.webp" class="opacity-img-slider"/>
            <div class="position-absolute text-weight-bolder f-size-2 text-color-white text-carousel">NOSSAS OBRAS</div>
            <button class="position-absolute text-weight-bold btn-swiper">
                <a href="#esconder-perfil4" class="tagA-no-style">
                    Ver Obras
                </a>
            </button>
        </div>
        <div class="swiper-slide position-relative">
            <img src="assets/index/bannersIniciais/3.webp" class="opacity-img-slider"/>
            <div class="position-absolute text-weight-bolder f-size-2 text-color-white text-carousel">ENTRE EM CONTATO</div>
            <button class="position-absolute text-weight-bold btn-swiper">
                <a href="#esconder-perfil6" class="tagA-no-style">
                    Ver Contatos
                </a>
            </button>
        </div>
    </div>
    <div class="swiper-button-next swiper-button"></div>
    <div class="swiper-button-prev swiper-button"></div>
    <div class="swiper-pagination"></div>
</div>

<div class="container-fluid background-default greyContainer" id="esconder-perfil2">
    <div class="row">
        <div class="text-center f-size-2 mt-4 text-weight-bold">
            NOSSOS SERVICOS
        </div>
    </div>

    <div class="row">
        <div class="col-md-1-odd"></div>
        <div class="col-md-3">
            <div class="card card-servicos mt-5">
                <div class="card-body text-center">
                    <div class="">
                        <div class="row">
                            <div class="col-4 col-md-4"></div>
                            <div class="col-4 col-md-4">
                                <img src="assets/index/icones/construcao.webp" class="card-img">
                            </div>
                            <div class="col-4 col-md-4"></div>
                        </div>
                    </div>
                    <div class="mt-3 text-weight-bolder color-red-reformular">
                        CONSTRUÇÃO
                    </div>
                    <div class="mt-3">
                        Realizamos obra da base ao acabamento
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-servicos mt-5">
                <div class="card-body text-center">
                    <div class="">
                        <div class="row">
                            <div class="col-4 col-md-4"></div>
                            <div class="col-4 col-md-4">
                                <img src="assets/index/icones/reformas.webp" class="card-img">
                            </div>
                            <div class="col-4 col-md-4"></div>
                        </div>
                    </div>
                    <div class="mt-3 text-weight-bolder color-red-reformular">
                        REFORMAS
                    </div>
                    <div class="mt-3">
                        Realizamos sua reforma do início ao fim
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-servicos mt-5">
                <div class="card-body text-center">
                    <div class="">
                        <div class="row">
                            <div class="col-4 col-md-4"></div>
                            <div class="col-4 col-md-4">
                                <img src="assets/index/icones/gerenciamento.webp" class="card-img">
                            </div>
                            <div class="col-4 col-md-4"></div>
                        </div>
                    </div>
                    <div class="mt-3 text-weight-bolder color-red-reformular">
                        GERENCIAMENTO DE OBRAS
                    </div>
                    <div class="mt-3">
                        Realizamos o gerenciamento e planejamento da sua obra
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1-odd"></div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid background-default redContainer" id="esconder-perfil3">
    <div class="row">
        <div class="text-center f-size-2 mt-4 text-weight-bold text-color-white">
            QUEM SOMOS
        </div>

        <div class="col-md-2"></div>
        <div class="col-md-3 mt-5">
            <img src="assets/index/biancaDanilo/12_square.webp" alt="" class="card-img">
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4 mt-5 centralizarObjetoCelular">
            <div class="f-size-1-3 text-color-white text-weight-bold">
                A Reformular Engenharia
            </div>
            <div class="f-size-1 text-color-white mt-3">
                Somos uma empresa especializada em obras de alto padrão. Nosso maior objetivo é proporcionar ao nosso cliente uma experiência única, conduzindo seu projeto com tranquilidade e confiança, pois entendemos que uma obra vai além de um serviço: é a realização de um sonho!
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
    <br>

    <div class="row">
        <div class="text-center f-size-2 mt-4 text-weight-bold text-color-white">
            QUEM IRÁ CUIDAR DO SEU SONHO
        </div>
    </div>

    <div class="row reverse">

        <div class="col-md-2"></div>
        <div class="col-md-4 mt-5 centralizarObjetoCelular">
            <div class="f-size-1-3 text-color-white text-weight-bold">
                Eduardo Braga
            </div>
            <div class="f-size-1 text-color-white mt-3">
                Engenheiro Civil, formado em 2019 pela Centro Universitário FAESA, atua há mais de 5 anos no mercado da construção civil. Já participou da execução de mais de 30.000 m² de obras em edifícios residenciais e se especializou em obras de alto padrão. É o responsável por garantir a qualidade e o atendimento de nossos clientes!
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3 mt-6">
            <img src="assets/index/eduardo2.webp" alt="" class="card-img">
        </div>
        <div class="col-md-2"></div>
    </div>
    <br><br><br>
</div>

<!-- Second Swiper -->
<div class="container-fluid background-default greyContainer mt-2" id="esconder-perfil4">
    
    <!-- ROW 01 -->
    <div class="row mx-0">
        <h1 class="text-center text-weight-bolder f-size-2 mt-4 mb-4">NOSSAS OBRAS</h1>

        <div class="col-md-1-odd"></div>

        <!-- 1 -->
        <div class="col-md-3 mt-2">
            <div class="text-weight-bolder color-red-reformular f-size-1-4 text-center">
                Leticia Gama
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper hover-images-swiper" onclick="createElementsSwiper(9, 'lg')">
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/1.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/3.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/4.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/5.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/6.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/7.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/8.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/LeticiaGama/9.webp" loading="lazy"/>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <!-- 2 -->
        <div class="col-md-3 swiper-card mt-2">
            <div class="text-weight-bolder color-red-reformular f-size-1-4 text-center">
                Jessica Puziol
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper hover-images-swiper" onclick="createElementsSwiper(18, 'jp')">
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/1.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/3.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/4.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/5.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/6.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/7.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/8.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/9.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL1/10.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/11.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/12.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/13.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/14.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/15.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/16.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/17.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/JessicaPuziolL2/18.webp" loading="lazy"/>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <!-- 3 -->
        <div class="col-md-3 swiper-card mt-2">
            <div class="text-weight-bolder color-red-reformular f-size-1-4 text-center">
                Karina Demoner
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper hover-images-swiper" onclick="createElementsSwiper(9, 'kd')">
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/1.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/3.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/4.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/5.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/6.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/7.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/8.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/KarinaDemoner/9.webp" loading="lazy"/>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <div class="col-md-1-odd"></div>
    </div>

    <!-- ROW 02 -->
    <div class="row mx-0 mt-4">

        <div class="col-md-1-odd"></div>

        <!-- 1 -->
        <div class="col-md-3 mt-3">
            <div class="text-weight-bolder color-red-reformular f-size-1-4 text-center">
                AP Barro Vermelho
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper hover-images-swiper" onclick="createElementsSwiper(15, 'bd')">
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/1.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/1_1_.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/3.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/4.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/4_4_.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/5.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/6.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/7.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/8.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/9.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/10.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/11.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/12.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/13.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/14.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/biancaDanilo/15.webp" loading="lazy"/>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <!-- 2 -->
        <div class="col-md-3 swiper-card mt-3">
            <div class="text-weight-bolder color-red-reformular f-size-1-4 text-center">
                AP Jardim da Penha
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper hover-images-swiper" onclick="createElementsSwiper(20, 'rr')">
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/1_1_.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/3.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/4.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/5.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/6.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/7.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/8.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/9.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/10.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/11.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/12.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/13.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/14.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/15.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/16.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/17.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/18.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/19.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/ReilaRenato/20.webp" loading="lazy"/>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <!-- 3 -->
        <div class="col-md-3 swiper-card mt-3">
            <div class="text-weight-bolder color-red-reformular f-size-1-4 text-center">
                AP Praia de Itaparica
            </div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper hover-images-swiper" onclick="createElementsSwiper(19, 'sr')">
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/1.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/2.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/3.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/4.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/5.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/6.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/7.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/8.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/9.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/10.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/11.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/12.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/13.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/14.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/15.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/16.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/17.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/18.webp" loading="lazy"/>
                    </div>
                    <div class="swiper-slide">
                        <img src="assets/index/SabrinaRomulo/19.webp" loading="lazy"/>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <div class="col-md-1-odd"></div>
    </div>

    <br><br><br>
</div>

<div class="container-fluid background-default redContainer centralizarObjetoCelular" id="esconder-perfil5">
    <div class="row">
        <div class="text-center f-size-2 mt-4 text-weight-bold text-color-white">
            O QUE FALAM SOBRE NÓS
        </div>
    </div>

    <div class="row reverse">

        <div class="col-md-2"></div>
        <div class="col-md-4 mt-5">
            <div class="f-size-1-3 text-color-white text-weight-bold">
                Jessica Puziol
            </div>
            <div class="f-size-1 text-color-white mt-3">
                Conheci o Eduardo em junho de 2020 e o contratei para a realização de um sonho: a abertura da minha segunda loja em Vila Velha.Confesso que quando peguei o orçamento, meu pai que sempre me ajuda a avaliar essas questões de obra, não queria fazer com ele; mas como tive problemas na execução da primeira loja (um projeto muito mais simples) decidi arriscar e fazer com ele: foi a melhor escolha que fiz. Ele cuidou de tudo para mim, me deixando tranquila para continuar a tocar meus negócios. <br><br> Em 2021, abrimos mais uma loja e Eduardo estava lá com a gente de novo. Super indico: atencioso antes, durante e depois da obra também.
                <br><br>
                <a href="https://www.instagram.com/jessicapuziol/" target="__blank" class="tagA-no-style">
                    <span class="iwt mt-1">
                        <i class="fa-brands fa-instagram"></i>
                        <span>jessicapuziol</span>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3 mt-6">
            <img src="assets/index/feedbacks/jessica.webp" alt="" class="card-img">
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-3 mt-6">
            <img src="assets/index/feedbacks/karina.webp" alt="" class="card-img">
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4 mt-5">
            <div class="f-size-1-3 text-color-white text-weight-bold">
                Karina Demoner 
            </div>
            <div class="f-size-1 text-color-white mt-3">
                Conheci o Eduardo em fevereiro de 2021 e o contratei para realizar o sonho de fazer meu sonho: o meu consultório do jeitinho que eu sempre sonhei! Não quis arriscar com outra pessoa pois tive boas referências sobre ele e nao tinha tempo pra me preocupar com obra. <br><br> E ele não me decepcionou: sempre se mostrou solícito e presente pra resolver qualquer problema da obra e pós obra também.
                <br><br>
                <a href="https://www.instagram.com/karinademoner/" target="__blank" class="tagA-no-style">
                    <span class="iwt mt-1">
                        <i class="fa-brands fa-instagram"></i>
                        <span>karinademoner</span>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row reverse">
        <div class="col-md-2"></div>
        <div class="col-md-4 mt-5">
            <div class="f-size-1-3 text-color-white text-weight-bold">
                Letícia Gama
            </div>
            <div class="f-size-1 text-color-white mt-3">
                Conheci o Dudu (que é como eu o chamo hoje em dia... rs), em julho de 2019 quando o contratei para realizar a obra de um dos maiores sonhos da minha vida, meu "Espaço Nails",  Na época, não o conhecia e confiei na indicação da minha arquiteta e,com certeza, foi a melhor escolha que fiz. <br> Ele ainda estava no início, mas sempre se mostrou disponível para resolver tudo (até depois do término da obra, sempre que precisei estava disponível), me deixando tranquila pra me dedicar às outras áreas da minha vida. <br><br> Hoje, sempre que tenho algo voltado "pra obra" sempre procuro o ele.
                <br><br>
                <a href="https://www.instagram.com/leticiagama_/" target="__blank" class="tagA-no-style">
                    <span class="iwt mt-1">
                        <i class="fa-brands fa-instagram"></i>
                        <span>leticiagama_</span>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3 mt-6">
            <img src="assets/index/feedbacks/leticia.webp" alt="" class="card-img">
        </div>
        <div class="col-md-2"></div>
    </div>

    <br><br><br>
</div>

<div class="container-fluid background-default greyContainer" id="esconder-perfil6">
    <div class="row text-center">
        <div class="text-center f-size-2 mt-4 text-weight-bold">
            ENTRE EM CONTATO
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-6 mt-1">
            <div class="f-size-1-2 text-weight-bold mt-3">
                <a href="mailto:contato@reformularengenharia.com" target="__blank" class="tagA-no-style red-hover">
                    <span class="iwt">
                        <i class="far fa-envelope"></i>
                        <span>contato@reformularengenharia.com</span>
                    </span>
                </a>
                <a href="https://hi.switchy.io/reformularsite" target="__blank" class="tagA-no-style red-hover">
                    <span class="iwt mt-1">
                        <i class="fa-brands fa-whatsapp"></i>
                        <span>(27) 99942-4067</span>
                    </span>
                </a>

                <a href="https://www.instagram.com/reformularengenharia/" target="__blank" class="tagA-no-style red-hover">
                    <span class="iwt mt-1">
                        <i class="fa-brands fa-instagram"></i>
                        <span>reformularengenharia</span>
                    </span>
                </a>
            </div>
        </div>
        <!-- <div class="col-md-5 mt-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.248273168228!2d-38.49153358552668!3d-3.7560420443647495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74963201865ff%3A0x2d77cda38272811f!2sIguatemi%20Bosque!5e0!3m2!1spt-BR!2sbr!4v1657723710460!5m2!1spt-BR!2sbr" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            <small class="mt-3 text-weight-bold">
                Av. Washington Soares, 85 - Edson Queiroz, Fortaleza - CE
            </small>
        </div> -->
        <div class="col-md-3"></div>
    </div>
    <br>
</div>

<div class="black-background background-click-swiper">

  <div class="row">
    <div class="col-10 col-md-11"></div>
    <div class="col-2 col-md-1 text-center p-2">
      <a class="click-swiper" href="#" onclick="fecharSwiperMaior()">
        <i class="fas fa-times"></i>
      </a>
    </div>
  </div>

  <div class="row">

    <div class="col-2 col-md-4"></div>
    <div class="col-8 col-md-4">
        <div class="text-weight-bolder text-color-white f-size-1-6 text-center" id="title-modal">
        </div>

        <div class="swiper mySwiper mt-4">
            <div class="swiper-wrapper" id="swiper-wrapper-modal">
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination"></div>
        </div>

    </div>
    <div class="col-2 col-md-4"></div>

  </div>

  <br><br><br><br>
  <br><br><br><br>

</div>