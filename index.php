<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <meta name="facebook-domain-verification" content="avuo63vn9ea2cpwlqjpuakz0macneh" />
    <title>REFORMULAR ENGENHARIA</title>
    <link rel="stylesheet" href="css/index/index.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W846CWC');</script>
    <!-- End Google Tag Manager -->
</head>

<?php require('default/top-nav.php'); ?>
<body id="remove-padding-top">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W846CWC"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- BOTAO DO WHTSAPP -->
    <a href="https://hi.switchy.io/reformularsite" class="float-element" target="_blank">
        <i class="fa-brands fa-whatsapp my-float"></i>
    </a>

    <?php require('contents/index_content.php'); ?>

    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>

        let esconderPerfil1 = document.getElementById("esconder-perfil1");
        let esconderPerfil2 = document.getElementById("esconder-perfil2");
        let esconderPerfil3 = document.getElementById("esconder-perfil3");
        let esconderPerfil4 = document.getElementById("esconder-perfil4");
        let esconderPerfil5 = document.getElementById("esconder-perfil5");
        let esconderPerfil6 = document.getElementById("esconder-perfil6");
        let esconderTopNav  = document.getElementById("esconder-topnav");
        let body            = document.getElementById("remove-padding-top");
        let swiperWraperMod = document.getElementById("swiper-wrapper-modal");
        let backgroundSwipe = document.querySelector(".background-click-swiper");

        var swiper = new Swiper(".mySwiper", {
            // effect: "coverflow",
            grabCursor: true,
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });

        function abrirSwiperMaior() {
            esconderPerfil1.classList.add('hide-on-click-swiper')
            esconderPerfil2.classList.add('hide-on-click-swiper')
            esconderPerfil3.classList.add('hide-on-click-swiper')
            esconderPerfil4.classList.add('hide-on-click-swiper')
            esconderPerfil5.classList.add('hide-on-click-swiper')
            esconderPerfil6.classList.add('hide-on-click-swiper')
            esconderTopNav.classList.add('hide-on-click-swiper')
            esconderFooter.classList.add('hide-on-click-swiper')

            body.classList.add('remove-ptop')

            // ADICIONA A CLASSE DE MOSTRAR
            backgroundSwipe.classList.add('show-on-click-swiper')
        }

        function fecharSwiperMaior() {
            esconderPerfil1.classList.remove('hide-on-click-swiper')
            esconderPerfil2.classList.remove('hide-on-click-swiper')
            esconderPerfil3.classList.remove('hide-on-click-swiper')
            esconderPerfil4.classList.remove('hide-on-click-swiper')
            esconderPerfil5.classList.remove('hide-on-click-swiper')
            esconderPerfil6.classList.remove('hide-on-click-swiper')
            esconderTopNav.classList.remove('hide-on-click-swiper')
            esconderFooter.classList.remove('hide-on-click-swiper')

            body.classList.remove('remove-ptop')

            // REMOVE A CLASSE DE MOSTRAR
            backgroundSwipe.classList.remove('show-on-click-swiper')
        }

        function createElementsSwiper(length, type) {
            if (type == 'rr') {
                folder = 'ReilaRenato';
                document.getElementById("title-modal").innerHTML = 'AP Jardim da Penha'
            } else if (type == 'lg') {
                folder = 'LeticiaGama';
                document.getElementById("title-modal").innerHTML = 'Leticia Gama'
            } else if (type == 'jp') {
                folder = 'JessicaPuziolL1';
                document.getElementById("title-modal").innerHTML = 'Jessica Puziol'
            } else if (type == 'kd') {
                folder = 'KarinaDemoner';
                document.getElementById("title-modal").innerHTML = 'Karina Demoner'
            } else if (type == 'sr') {
                folder = 'SabrinaRomulo';
                document.getElementById("title-modal").innerHTML = ' AP Praia de Itaparica'
            } else if (type == 'bd') {
                folder = 'biancaDanilo';
                document.getElementById("title-modal").innerHTML = 'AP Barro Vermelho'
            }

            path = 'assets/index/' + folder + '/';
            for (var i = 1; i < (length + 1); i++) {

                if (type == 'jp' && i > 10) {
                    folder = 'JessicaPuziolL2'
                    path = 'assets/index/' + folder + '/';
                } else if (type == 'bd' || type == 'rr') {
                    if (i == 1) {
                        let divRow = document.createElement('div');
                        divRow.className = 'swiper-slide';

                        let img = document.createElement('img');
                        img.src = path + '1_1_.webp';
                    }
                } else if (type == 'bd') {
                    if (i == 4) {
                        let divRow = document.createElement('div');
                        divRow.className = 'swiper-slide';

                        let img = document.createElement('img');
                        img.src = path + '4_4_.webp';
                    }
                }

                let divRow = document.createElement('div');
                divRow.className = 'swiper-slide';

                let img = document.createElement('img');
                img.src = path + i + '.webp';

                divRow.append(img);

                swiperWraperMod.append(divRow);
            }

            abrirSwiperMaior()
        }

    </script>

    <script>
    let clickedBars = false

    function menuResponsivo() {
        navbar = document.getElementById("navbarNav")

        clickedBars = !clickedBars
        if (clickedBars) {
            navbar.classList.add("display-block-important")
        } else {
            navbar.classList.remove("display-block-important")
        }
    }

    </script>
</body>

<?php require('default/footer.php'); ?>

<script>
    let esconderFooter = document.getElementById("esconder-footer");
</script>

</html>