<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted" id="esconder-footer">
  <!-- Copyright -->
  <div class="p-3 text-color-white" style="background-color: #BA1E23;">
    <div class="row">
      <div class="col-4 col-md-5"></div>
      <div class="col-4 col-md-2">
        <img src="assets/logoSecundariaReformularBranco.webp" class="card-img" alt="">
      </div>
      <div class="col-4 col-md-5"></div>
    </div>
    <div class="f-size-1 text-center mt-3">
      <span class="text-weight-bolder">REFORMULAR ENGENHARIA</span> | CNPJ: 33.293.364/0001-66 | contato@reformularengenharia.com | Todos os direitos reservados
    </div>

  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->