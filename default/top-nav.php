<header>

    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light" id="esconder-topnav">
        <a class="navbar-brand p-2" href="#">
            <img src="assets/iconeVermelho.webp" width="40" height="40" class="d-inline-block align-top" alt="">
            <span class="p-2 align-vertical-logo text-weight-bold color-red-reformular logo-title">REFORMULAR ENGENHARIA</span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"  onclick="menuResponsivo()">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto p-3">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Início</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#esconder-perfil2">Nossos Serviços</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#esconder-perfil4">Nossas Obras</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#esconder-perfil5">Feedbacks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#esconder-perfil3">Sobre Nós</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#esconder-perfil6">Contatos</a>
                </li>
            </ul>
        </div>
    </nav>

</header>